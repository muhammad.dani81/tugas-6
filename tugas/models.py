from django.db import models

class StatusModel(models.Model):
	status = models.CharField(max_length=300)
	date = models.DateField(auto_now= True)