from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static
from .views import status, delete_status

urlpatterns = [
	path('', status, name='status'),
	path('delete_status/', delete_status, name='delete status')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)