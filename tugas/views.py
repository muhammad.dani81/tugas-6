from django.shortcuts import render, redirect
from django.http import HttpResponse
from . import models, forms

def status(request):
	form = forms.StatusForm()
	if request.method == 'POST':
		form = forms.StatusForm(request.POST)
		if form.is_valid():
			status = models.StatusModel(
				status = form.cleaned_data['status']
			)
			status.save()
			
	status = models.StatusModel.objects.all()

	return render(request, 'tugas6.html', {'form': form, 'status': status})

def delete_status(request):
	models.StatusModel.objects.all().delete()
	return redirect('../')
