import unittest
import time

from django.test import TestCase, Client
from django.urls import resolve

from selenium import webdriver
from selenium.webdriver.firefox.options import Options

from .models import StatusModel
from .forms import StatusForm
from .views import status, delete_status

class UnitTest(TestCase):

	def test_url_exists(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
	def test_url_that_does_not_exist(self):
		response = Client().get('/nothing/')
		self.assertEqual(response.status_code, 404)

	def test_delete_status_redirect(self):
		response = Client().get('/delete_status/')
		self.assertEqual(response.status_code, 302)

	def test_use_form_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'tugas6.html')

	def test_halo_is_inside_template(self):
		response = Client().get('/')
		content = response.content.decode('utf8')
		self.assertIn('Halo, apa kabar?', content)
	def test_form_is_inside_template(self):
		response = Client().get('/')
		content = response.content.decode('utf8')
		self.assertIn('<form', content)
	def test_button_is_inside_template(self):
		response = Client().get('/')
		content = response.content.decode('utf8')
		self.assertIn('<button', content)
		self.assertIn('Submit', content)

	def test_page_uses_views_function(self):
		found = resolve('/')
		self.assertEqual(found.func, status) #Views's function
	def test_delete_status_function(self):
		found = resolve('/delete_status/')
		self.assertEqual(found.func, delete_status)

	def test_status_form(self):
		form = StatusForm(data={'status':"Coba Coba Huehue"}) #Form's class
		self.assertTrue(form.is_valid())
		status = form.cleaned_data['status']
		self.assertEqual(status, "Coba Coba Huehue")
	def test_blank_input(self):
		form = StatusForm(data={'status':""})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['status'],
			["This field is required."]
		)

	def test_instance_create_model(self):
		test_instance = StatusModel.objects.create(status="Coba coba")
		self.assertTrue(isinstance(test_instance, StatusModel))
	def test_model(self):
		StatusModel.objects.create(status='Status', date='Kencan huehue')
		self.assertEqual(StatusModel.objects.all().count(), 1)

class SeleniumFunctionalTest(TestCase):

	def setUp(self):
		options = Options()
		options.add_argument('--headless')
		self.selenium = webdriver.Firefox(firefox_options=options)
		super(SeleniumFunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(SeleniumFunctionalTest,self).tearDown()

	def test_inserting_data_to_form(self):
		#Inserting one data
		self.selenium.get('http://127.0.0.1:8000/')
		time.sleep(10)

		form_status = self.selenium.find_element_by_id("status")
		form_status.send_keys("Coba coba")
		button_status = self.selenium.find_element_by_id("submit")
		button_status.click()
		time.sleep(10)

		self.assertIn("Coba coba", self.selenium.page_source)

	def test_deleting_all_data(self):
		self.selenium.get('http://127.0.0.1:8000/')
		time.sleep(10)

		form_status = self.selenium.find_element_by_id("status")
		form_status.send_keys("Coba coba")
		button_status = self.selenium.find_element_by_id("submit")
		button_status.click()
		time.sleep(10)

		self.assertIn("Coba coba", self.selenium.page_source)
		time.sleep(10)

		button_delete = self.selenium.find_element_by_id("delete")
		button_delete.click()
		time.sleep(10)

		self.assertNotIn("Coba coba", self.selenium.page_source)